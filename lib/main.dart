import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stock_control/database/dao/stock_dao.dart';
import 'package:stock_control/database/database.dart';
import 'package:stock_control/models/stock.dart';
import 'package:stock_control/screens/stock_statement.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final sharedPreferences = await SharedPreferences.getInstance();
  runApp(MyApp(sharedPreferences));
  getDatabase().then((onValue) => debugPrint('works'));
  StockDAO stockDAO = StockDAO();
  stockDAO.save(Stock(0, 'AMBEV', 'ABEV4'));
  List<Stock> stocks = await stockDAO.findAll();
  debugPrint(stocks.toString());
//  TradeStockDAO tradeStockDAO = TradeStockDAO();
//  tradeStockDAO.save(TradeStock(
//      0, MovimentType.buy, Stock(0, '', ''), 10, 13.01, DateTime.now()));
//  List<TradeStock> extract = await tradeStockDAO.findAll();
//  debugPrint(extract.toString());
}

class MyApp extends StatelessWidget {
  final SharedPreferences sharedPreferences;
  MyApp(this.sharedPreferences);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: StockStatement(),
    );
  }
}
