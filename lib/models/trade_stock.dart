import 'package:stock_control/constants/enums.dart';
import 'package:stock_control/models/stock.dart';

class TradeStock {
  final int id;
  final MovimentType movimentType;
  final Stock stock;
  final int quantity;
  final double price;
  final DateTime tradeDate;

  TradeStock(
    this.id,
    this.movimentType,
    this.stock,
    this.quantity,
    this.price,
    this.tradeDate,
  );

  @override
  String toString() {
    return 'TradeStock{movimentType: $movimentType, stock: $stock, quantity: $quantity, price: $price, tradeDate: $tradeDate}';
  }
}
