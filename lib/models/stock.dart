class Stock {
  final int id;
  final String code;
  final String name;

  Stock(this.id, this.name, this.code);

  @override
  String toString() {
    return 'Stock{id: $id, code: $code, name: $name}';
  }
}
