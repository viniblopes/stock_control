class StockBroker {
  String name;
  String bankName;
  int bankNumber;
  int accountNumber;

  StockBroker({this.name, this.bankName, this.bankNumber, this.accountNumber});

  @override
  String toString() {
    return 'StockBroker{name: $name, bankName: $bankName, bankNumber: $bankNumber, accountNumber: $accountNumber}';
  }
}
