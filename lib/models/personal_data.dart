class PersonalData {
  String name;
  String cpf;

  PersonalData({this.name, this.cpf});

  @override
  String toString() {
    // TODO: implement toString
    return '{' + name + ', ' + cpf + '}';
  }
}
