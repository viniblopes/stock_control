import 'package:flutter/material.dart';
import 'package:stock_control/constants/enums.dart';
import 'package:stock_control/models/trade_stock.dart';

class TradeStockItem extends StatelessWidget {
  final TradeStock tradeStock;
  final Function onTap;
  final Function onPressedTrailing;

  TradeStockItem({this.tradeStock, this.onTap, this.onPressedTrailing});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap: onTap,
        leading: Icon(
          tradeStock.movimentType == MovimentType.buy
              ? Icons.add
              : Icons.remove,
          size: 36,
        ),
        title: Text(tradeStock.stock.name),
        trailing: FlatButton.icon(
          onPressed: onPressedTrailing,
          icon: Icon(Icons.edit),
          label: Text(""),
        ),
        subtitle: Text("R\$ " + tradeStock.price.toStringAsFixed(2)),
      ),
    );
  }
}
