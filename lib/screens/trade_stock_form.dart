import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:stock_control/utils/CurrencyPtBrInputFormatter.dart';

class TradeStockForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      minimum: EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(labelText: 'Código da Ação'),
            keyboardType: TextInputType.text,
            style: TextStyle(
              fontSize: 20.0,
            ),
            textCapitalization: TextCapitalization.characters,
            maxLength: 6,
          ),
          TextField(
            decoration: InputDecoration(labelText: 'Quantidade'),
            keyboardType: TextInputType.number,
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          TextField(
            decoration: InputDecoration(labelText: 'Preço'),
            keyboardType: TextInputType.number,
            style: TextStyle(
              fontSize: 20.0,
            ),
            inputFormatters: [
              WhitelistingTextInputFormatter.digitsOnly,
              CurrencyPtBrInputFormatter(),
            ],
          ),
          TextField(
            decoration: InputDecoration(labelText: 'Data da Compra/Venda'),
            keyboardType: TextInputType.datetime,
            style: TextStyle(
              fontSize: 20.0,
            ),
            inputFormatters: [
              MaskTextInputFormatter(
                  mask: '##/##/####', filter: {"#": RegExp(r'[0-9]')})
            ],
          ),
        ],
      ),
    );
  }
}
