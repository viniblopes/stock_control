import 'package:flutter/material.dart';
import 'package:stock_control/constants/enums.dart';
import 'package:stock_control/models/stock.dart';
import 'package:stock_control/models/trade_stock.dart';
import 'package:stock_control/screens/home.dart';
import 'package:stock_control/screens/shared_widgets/trade_stock_item.dart';
import 'package:stock_control/screens/trade_stock_form.dart';

class StockStatement extends StatefulWidget {
  @override
  _StockStatementState createState() => _StockStatementState();
}

class _StockStatementState extends State<StockStatement> {
  int _selectedIndex = 0;

  List<Widget> _widgets = <Widget>[
    Home(),
    ListTradeStock(),
    TradeStockForm(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista de Ações"),
      ),
      body: _widgets.elementAt(_selectedIndex),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print("teste");
        },
        backgroundColor: Colors.red,
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance),
            title: Text('Extrato'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_circle),
            title: Text('Adicionar'),
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}

class ListTradeStock extends StatelessWidget {
  final List<TradeStock> tradeList = new List<TradeStock>();
  @override
  Widget build(BuildContext context) {
//    addTestStocks();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: tradeList.length,
      itemBuilder: (BuildContext context, int index) =>
          buildItem(context, index),
    );
  }

  Widget buildItem(BuildContext context, int index) {
    TradeStock tradeStock = this.tradeList.elementAt(index);
    return TradeStockItem(
      tradeStock: tradeStock,
      onPressedTrailing: () =>
          print("Tocou no elemento" + tradeStock.toString()),
    );
  }

  void addTestStocks() {
    for (int i = 0; i < 200000; i++) {
      TradeStock trade = TradeStock(
        i,
        i % 2 == 0 ? MovimentType.buy : MovimentType.sell,
        Stock(0, 'Ambev', 'ABEV3'),
        10 + i,
        13.30 + i,
        DateTime.now(),
      );
      this.tradeList.add(trade);
    }
  }
}
