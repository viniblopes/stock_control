import 'package:sqflite/sqflite.dart';
import 'package:stock_control/database/dao/stock_dao.dart';
import 'package:stock_control/database/database.dart';
import 'package:stock_control/models/trade_stock.dart';

class TradeStockDAO {
  static const String tableSql = 'CREATE TABLE $_tableName('
      ' $_id INTEGER PRIMARY KEY,'
      ' $_movimentType INTEGER NOT NULL,'
      ' $_quantity INTEGER NOT NULL,'
      ' $_price REAL NOT NULL,'
      ' $_tradeDate INTEGER NOT NULL'
      ' $_stockId INTEGER'
      ' CONSTRAINT $_fkStock'
      ' FOREIGN KEY ($_stockId)'
      ' REFERENCES ${StockDAO.tableName}($_id)'
      ')';
  static const String _tableName = 'trade_stocks';
  static const String _id = 'id';
  static const String _movimentType = 'moviment_type';
  static const String _fkStock = 'fk_stock';
  static const String _stockId = 'stock_id';
  static const String _quantity = 'quantity';
  static const String _price = 'price';
  static const String _tradeDate = 'trade_date';

  Future<int> save(TradeStock tradeStock) async {
    final Database db = await getDatabase();
    Map<String, dynamic> tradeStockMap = _toMap(tradeStock);
    return db.insert(_tableName, tradeStockMap);
  }

  Map<String, dynamic> _toMap(TradeStock tradeStock) {
    final Map<String, dynamic> tradeStockMap = Map();
    tradeStockMap[_movimentType] = tradeStock.movimentType.index;
    tradeStockMap[_stockId] = tradeStock.stock.id;
    tradeStockMap[_quantity] = tradeStock.quantity;
    tradeStockMap[_price] = tradeStock.price;
    tradeStockMap[_tradeDate] = tradeStock.tradeDate.toIso8601String();
    return tradeStockMap;
  }

  Future<List<TradeStock>> findAll() async {
    final Database db = await getDatabase();
    //TODO: Change query to join to Table StockDAO._tableName;
    final List<Map<String, dynamic>> result = await db.query(_tableName);
    db.rawQuery('SELECT '
        '$_tableName.$_id,'
        '$_tableName.$_movimentType,'
        '$_tableName.$_stockId,'
        '${StockDAO.tableName}.${StockDAO.code},'
        '${StockDAO.tableName}.${StockDAO.name},'
        '$_tableName.$_quantity,'
        '$_tableName.$_price,'
        '$_tableName.$_tradeDate'
        ' FROM $_tableName'
        'INNER JOIN ${StockDAO.tableName} ON'
        '${StockDAO.tableName}.${StockDAO.id} = $_tableName.$_stockId');
    List<TradeStock> trades = _toList(result);
    return trades;
  }

  List<TradeStock> _toList(List<Map<String, dynamic>> result) {
    final List<TradeStock> stocks = List();
    for (Map<String, dynamic> row in result) {
      final TradeStock contact = TradeStock(
        row[_id],
        row[_movimentType],
        row[_stockId],
        row[_quantity],
        row[_price],
        row[_tradeDate],
      );
      stocks.add(contact);
    }
    return stocks;
  }
}
