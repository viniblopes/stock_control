import 'package:sqflite/sqflite.dart';
import 'package:stock_control/database/database.dart';
import 'package:stock_control/models/stock.dart';

class StockDAO {
  static const String tableSql = 'CREATE TABLE $tableName('
      '$_id INTEGER PRIMARY KEY, '
      '$_name TEXT NOT NULL, '
      '$_code TEXT NOT NULL UNIQUE)';
  static const String tableName = 'stocks';

  static const String _id = 'id';
  static const String _name = 'name';
  static const String _code = 'code';

  Future<int> save(Stock stock) async {
    final Database db = await getDatabase();
    Map<String, dynamic> stockMap = _toMap(stock);
    return db.insert(tableName, stockMap);
  }

  Map<String, dynamic> _toMap(Stock stock) {
    final Map<String, dynamic> stockMap = Map();
    stockMap[_name] = stock.name;
    stockMap[_code] = stock.code;
    return stockMap;
  }

  Future<List<Stock>> findAll() async {
    final Database db = await getDatabase();
    final List<Map<String, dynamic>> result = await db.query(tableName);
    List<Stock> stocks = _toList(result);
    return stocks;
  }

  List<Stock> _toList(List<Map<String, dynamic>> result) {
    final List<Stock> stocks = List();
    for (Map<String, dynamic> row in result) {
      final Stock stock = Stock(
        row[_id],
        row[_name],
        row[_code],
      );
      stocks.add(stock);
    }
    return stocks;
  }

  static String get code => _code;

  static String get name => _name;

  static String get id => _id;
}
