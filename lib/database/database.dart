import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'dao/stock_dao.dart';

Future<Database> getDatabase() async {
  final String path = join(await getDatabasesPath(), 'stockControl.db');
  return openDatabase(
    path,
    onCreate: (db, version) async {
      await db.execute(StockDAO.tableSql);
//      await db.execute(TradeStockDAO.tableSql);
    },
    version: 1,
    onDowngrade: onDatabaseDowngradeDelete,
  );
}
